package com.demo.controller;

import com.demo.entity.Book;
import com.demo.service.BookService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author tiger
 */
@RestController
public class BookController {
    private final Log log = LogFactory.getLog(BookController.class);
    @Autowired
    BookService bookService;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @GetMapping("/redis")
    public void bookOps() {

        ValueOperations<String, String> ops1 = stringRedisTemplate.opsForValue();
        ops1.set("name", "三国演义");
        System.out.println("redis->name值:" + ops1.get("name"));

        ValueOperations<String, Object> ops2 = redisTemplate.opsForValue();
        Book book = new Book();
        book.setId(1);
        book.setName("西厢记");
        book.setAuthor("王实甫");
        ops2.set("book", book);
        book = (Book) ops2.get("book");
        System.out.println("redis->book值:" + book);
    }

    @GetMapping("/save")
    public void saveSession(String name, HttpSession session) {
        session.setAttribute("name", name);
    }

    @GetMapping("/get")
    public void getSession(HttpSession session) {
        System.out.println(session.getAttribute("name").toString());
    }
}
