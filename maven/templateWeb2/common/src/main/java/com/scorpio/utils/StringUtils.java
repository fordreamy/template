package com.scorpio.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    /**
     * @param strDate
     * @param pattern 格式如:yyyy-MM-dd hh:mm:ss:SSS 年月日时分秒毫秒
     * @return
     */
    public static Date stringToDate(String strDate, String pattern) {
        Date date = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            date = simpleDateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 根据正则匹配全部内容
     *
     * @param content
     * @param regex
     * @return
     */
    public static boolean isMatched(String content, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        return matcher.matches();
    }

    /**
     * 根据正则找到匹配的子序列
     *
     * @param content
     * @param regex
     * @return
     */
    public static boolean isFindMatched(String content, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        return matcher.find();
    }

    /**
     * 返回正则匹配到的字符串
     *
     * @param content
     * @param regex
     * @return
     */
    public static String firstMatchedString(String content, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }

    /**
     * 替换所有匹配到的子序列
     * @param content
     * @param regex
     * @param replacement
     * @return
     */
    public static String replaceAll(String content, String regex, String replacement) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        content = matcher.replaceAll(replacement);
        return content;
    }

    /**
     * 匹配到的次数
     *
     * @param content
     * @param regex
     * @return
     */
    public static int matchedCount(String content, String regex) {
        // 按指定模式在字符串查找
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content); // 获取 matcher 对象
        int count = 0;

        while (matcher.find()) {
            count++;
            System.out.println("Match number " + count);
            System.out.println("start(): " + matcher.start());
            System.out.println("end(): " + matcher.end());
        }
        return count;
    }

    /**
     * 正则匹配到的字符串的开始下标
     *
     * @param content
     * @param regex
     * @return
     */
    public static int matchedStringStartIndex(String content, String regex) {
        String m = firstMatchedString(content, regex);
        return m != "" ? content.indexOf(m) : -1;
    }

    /**
     * 正则匹配到的字符串的结束下标
     *
     * @param content
     * @param regex
     * @return
     */
    public static int matchedStringEndIndex(String content, String regex) {
        String m = firstMatchedString(content, regex);
        return m != "" ? matchedStringStartIndex(content, regex) + m.length() : -1;
    }

    public static String convertBytesToHex(byte[] value) {
        int len = value.length;
        char[] buff = new char[len + len];
        char[] hex = "0123456789abcdef".toCharArray();
        for (int i = 0; i < len; i++) {
            int c = value[i] & 0xff;
            buff[i + i] = hex[c >> 4];
            buff[i + i + 1] = hex[c & 0xf];
        }
        return new String(buff);
    }

    public static String convertBytesToHex2(byte[] value){
        StringBuffer strHex = new StringBuffer();
        for (int i = 0; i < value.length; i++)
        {
            String hex = Integer.toHexString(0xff & value[i]);
            if (hex.length() == 1)
            {
                strHex.append('0');
            }
            strHex.append(hex);
        }
        return strHex.toString();
    }

    public static void main(String[] args) {

    }
}
