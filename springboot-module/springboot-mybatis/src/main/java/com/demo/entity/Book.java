package com.demo.entity;

import lombok.Data;

/**
 * @author tiger
 */
@Data
public class Book {
    private Integer id;
    private String name;
    private String author;

    public Book() {
    }

    public Book(Integer id, String name, String author) {
        this.id = id;
        this.name = name;
        this.author = author;
    }
}
