package com.demo.controller;

import com.demo.service.UserService;
import com.demo.entity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {
    private Log log = LogFactory.getLog(UserController.class);
    @Autowired
    UserService userService;

    @RequestMapping("/")
    public String home() {
        log.info("进入首页");
        return "index.html";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/post/user/{id}")
    @ResponseBody
    public User getUserByIdPost(@PathVariable Long id) {
        return userService.geUserById(id);
    }
}
