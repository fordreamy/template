package com.demo.dao;

import com.demo.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author tiger
 */
public interface BookDao extends JpaRepository<Book, Integer> {
    /**
     * 根据书名查询
     *
     * @param name 书名
     * @return 书列表
     */
    @Query(value = "select * from book where name like %?1% ", nativeQuery = true)
    List<Book> getBookByName(String name);
}
