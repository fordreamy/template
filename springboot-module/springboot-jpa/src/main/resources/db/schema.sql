drop table user if exists;
CREATE TABLE user
(
    id            int(11),
    name          varchar(45),
    department_id int(11),
    create_time   varchar(50),
    address       varchar(50),
    PRIMARY KEY (id)
);

drop table department if exists;
CREATE TABLE department
(
    id   int(11),
    name varchar(45),
    PRIMARY KEY (id)
);

drop table book if exists;
CREATE TABLE book
(
    id     int(11),
    name   varchar(100),
    author varchar(100),
    PRIMARY KEY (id)
);
