package com.demo.controller;

import com.demo.entity.Book;
import com.demo.service.BookService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author tiger
 */
@RestController
public class BookController {
    private final Log log = LogFactory.getLog(BookController.class);
    @Autowired
    BookService bookService;

    @GetMapping("/bookOps")
    public void bookOps() {
        System.out.println("*************Book列表*************" + bookService.getAllBooks());
        System.out.println(bookService.getAllBooks());

        Book b1 = new Book();
        b1.setId(1);
        b1.setName("西厢记");
        b1.setAuthor("王实甫");
        int add = bookService.addBook(b1);
        System.out.println("新增>>" + add);
        System.out.println("*************Book列表*************" + bookService.getAllBooks());
        System.out.println(bookService.getAllBooks());

        Book b2 = new Book();
        b2.setId(1);
        b2.setName("朝花夕拾");
        b2.setAuthor("鲁迅");
        int update = bookService.updateBook(b2);
        System.out.println("更新>>" + update);
        System.out.println("*************Book列表*************" + bookService.getAllBooks());
        System.out.println(bookService.getAllBooks());

        int delete = bookService.deleteBookById(1);
        System.out.println("删除>>" + delete);
        System.out.println("*************Book列表*************" + bookService.getAllBooks());
        System.out.println(bookService.getAllBooks());
    }

    @GetMapping("/books")
    @ResponseBody
    public List<Book> allBookList() {
        bookService.deleteBookById(1);
        Book b1 = new Book(1, "西厢记", "王实甫");
        bookService.addBook(b1);
        return bookService.getAllBooks();
    }
}
