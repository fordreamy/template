package initDB;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * 数据库工具类
 * 
 * @author
 *
 */
public class DBConnection {

	/**
	 * 获取数据库连接
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Connection getConnection(String driverClassName,String url, String username, String password) throws Exception {
		Class.forName(driverClassName);
		Connection con = DriverManager.getConnection(url, username, password);
		return con;
	}

	/**
	 * 关闭数据库连接
	 * 
	 * @param con
	 * @throws Exception
	 */
	public void closeCon(Connection con) throws Exception {
		if (con != null) {
			con.close();
		}
	}

	public static void main(String[] args) {
		try {
			getConnection("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/test_01", "root", "123456");
			System.out.println("数据库连接成功");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("数据库连接失败");
		}
	}
}
