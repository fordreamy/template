package com.demo.dao;

import com.demo.entity.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author tiger
 */
@Mapper
public interface BookMapper {
    int addBook(Book book);

    int updateBookById(Book book);

    int deleteBookById(Integer id);

    Book getBookById(Integer id);

    List<Book> getAllBooks();
}
