package com.demo;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class Test02ApplicationTests {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void controllerTestGet2() throws Exception {
        ResponseEntity<String> books = testRestTemplate.getForEntity("/books?name={0}",
                String.class, "Michael");
        System.out.println(books.getBody());
    }

}
