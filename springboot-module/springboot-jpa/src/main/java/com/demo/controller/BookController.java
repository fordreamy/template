package com.demo.controller;

import com.demo.entity.Book;
import com.demo.service.BookService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tiger
 */
@RestController
public class BookController {
    private final Log log = LogFactory.getLog(BookController.class);
    @Autowired
    BookService bookService;

    @GetMapping("/bookOps")
    public void bookOps() {

        System.out.println("*************Book列表*************");
        System.out.println(bookService.getAllBooks());

        System.out.println("新增>>");
        Book book = new Book();
        book.setName("西厢记");
        book.setAuthor("王实甫");
        bookService.addBook(book);
        System.out.println("*************新增记录*************");
        System.out.println(book);

        System.out.println("更新>>");
        book = new Book();
        book.setId(1);
        book.setName("朝花夕拾");
        book.setAuthor("鲁迅");
        bookService.updateBook(book);
        System.out.println("*************更新记录*************");
        System.out.println(book);

        System.out.println("书名查询>>");
        System.out.println(bookService.getBookByName("西"));

    }

    @GetMapping("/list")
    public void findAll() {
        PageRequest pageRequest = PageRequest.of(1, 2);
        Page<Book> page = bookService.getBookByPage(pageRequest);
        System.out.println("总页数:" + page.getTotalPages());
        System.out.println("总记录数:" + page.getTotalElements());
        System.out.println("查询结果:" + page.getContent());
        System.out.println("当前页数:" + page.getNumber());
        System.out.println("当前页记录数:" + page.getNumberOfElements());
        System.out.println("页记录数:" + page.getSize());
    }
}
