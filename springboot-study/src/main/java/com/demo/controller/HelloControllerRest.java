package com.demo.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;

/**
 * @author tiger
 */
@RestController
public class HelloControllerRest {

    private final Log logger = LogFactory.getLog(this.getClass());

    @GetMapping("/hello")
    public String hello() {
        logger.info("进入rest");
        return "hello";
    }
}
