package com.demo.web.controller.mvc;

import com.demo.web.entity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @GetMapping("/login")
    public String home(User user, HttpServletRequest request) {
        if ("jack".equals(user.getName())) {
            logger.info("登陆成功!");
            request.getSession().setAttribute("user", user);
            return "home";
        }
        return "login";
    }

    @GetMapping("/")
    public String index() {
        logger.info("Greeting");
        return "login";
    }
}
