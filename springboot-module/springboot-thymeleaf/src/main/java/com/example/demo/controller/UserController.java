package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {

    @GetMapping("model")
    public String hello(Model model) {
        model.addAttribute("hello", "hello model");
        return "hello";
    }

    @GetMapping("modelMap")
    public String hello(ModelMap modelMap) {
        modelMap.put("hello", "hello modelMap");
        return "hello";
    }

}
