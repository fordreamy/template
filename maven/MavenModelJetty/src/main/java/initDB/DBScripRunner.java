package initDB;

import java.io.*;
import java.sql.Connection;

import com.ibatis.common.jdbc.ScriptRunner;

import initDB.DBConnection;
import initDB.initOracle.InitOracle;

/**
 * 根据脚本执行sql,支持MySQL、Oracle
 */
public class DBScripRunner {

	/**
	 * 执行mysql脚本
	 * @throws Exception
	 */
	public static void sqlBatch(String filePath,Connection connection)  {
		
		try {
			System.out.println("驱动名字:"+connection.getMetaData().getDriverName());
			System.out.println("驱动版本:"+connection.getMetaData().getDriverVersion());
			System.out.println("URL:"+connection.getMetaData().getURL());
			System.out.println("用户名:"+connection.getMetaData().getUserName());

			Reader reader = new BufferedReader(new FileReader(filePath));
			ScriptRunner runner = new ScriptRunner(connection, false, false);
			runner.runScript(reader);
		} catch (Exception e) {
			System.out.println("执行脚本出错!");
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws Exception {
		System.out.println("类根目录:" + ClassLoader.getSystemResource(""));
		System.out.println("当前类路径:" + InitOracle.class.getResource("").getPath()); // Class文件所在路径
		
		String filePath = InitOracle.class.getResource("").getPath() + "initOracle.sql";
		
		Connection conn = DBConnection.getConnection("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@210.25.24.55:1521:myorcl","ywxt2", "ywxt2");
		
		sqlBatch(filePath,conn);
	}

}
