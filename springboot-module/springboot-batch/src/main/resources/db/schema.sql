drop table user if exists;
CREATE TABLE user
(
    id       int(11),
    username varchar(45),
    address  varchar(255),
    gender   varchar(255),
    PRIMARY KEY (id)
);
