--------------------------------------------------------
--  drop删除表时，表不存在则报错，但是会继续执行
--------------------------------------------------------

drop table "test";

CREATE TABLE "test" 
   (	"USERID" VARCHAR2(50 CHAR), 
	"AGE" NUMBER(10,0), 
	"EMAIL" VARCHAR2(100 CHAR), 
	"USERPASSWORD" VARCHAR2(50 CHAR), 
	"PHONE" VARCHAR2(100 CHAR), 
	"REGDATE" TIMESTAMP (6), 
	"SALARY" FLOAT(126), 
	"USERCODE" VARCHAR2(200 CHAR), 
	"USERNAME" VARCHAR2(200 CHAR)
   );

drop table "test1";

CREATE TABLE "test1" 
   (	"USERID" VARCHAR2(50 CHAR), 
	"AGE" NUMBER(10,0), 
	"EMAIL" VARCHAR2(100 CHAR), 
	"USERPASSWORD" VARCHAR2(50 CHAR), 
	"PHONE" VARCHAR2(100 CHAR), 
	"REGDATE" TIMESTAMP (6), 
	"SALARY" FLOAT(126), 
	"USERCODE" VARCHAR2(200 CHAR), 
	"USERNAME" VARCHAR2(200 CHAR)
   );