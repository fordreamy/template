INSERT INTO user
VALUES (1, 'root', '$2a$10$RMuFXGQ5AtH4wOvkUqyvuecpqUSeoxZYqilXzbz50dceRsga.WYiq', 1, 0);
INSERT INTO user
VALUES (2, 'admin', '$2a$10$RMuFXGQ5AtH4wOvkUqyvuecpqUSeoxZYqilXzbz50dceRsga.WYiq', 1, 0);
INSERT INTO user
VALUES (3, 'sang', '$2a$10$RMuFXGQ5AtH4wOvkUqyvuecpqUSeoxZYqilXzbz50dceRsga.WYiq', 1, 0);
INSERT INTO menu
VALUES (1, '/db/**');
INSERT INTO menu
VALUES (2, '/admin/**');
INSERT INTO menu
VALUES (3, '/user/**');
INSERT INTO menu_role
VALUES (1, 1, 1);
INSERT INTO menu_role
VALUES (2, 2, 2);
INSERT INTO menu_role
VALUES (3, 3, 3);
INSERT INTO role
VALUES (1, 'dba', '数据库管理员');
INSERT INTO role
VALUES (2, 'admin', '系统管理员');
INSERT INTO role
VALUES (3, 'user', '用户');
INSERT INTO user_role
VALUES (1, 1, 1);
INSERT INTO user_role
VALUES (2, 1, 2);
INSERT INTO user_role
VALUES (3, 2, 2);
INSERT INTO user_role
VALUES (4, 3, 3);
