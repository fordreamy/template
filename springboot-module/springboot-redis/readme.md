## CentOS7 Redis 安装
```shell script
tar -zxvf redis*.tar.gz
cd redis*
make MALLOC=libc
make install
```
若在执行 make MALLOC=libc 命令时提示"gcc未找到命令"，则先安装 gcc。`yum install gcc`

## 配置Redis之redis.conf
```
# 允许Redis在后台启动
daemonizeyes
# 允许连接该 Redis实例的地址，默认情况下只允许本地连接
bind 127.0.0.1
# 登录该 Redis 实例所需的密码
requirepass 123456
# 配置了密码登录就可以关闭保护模式了
protected-mode no
```

