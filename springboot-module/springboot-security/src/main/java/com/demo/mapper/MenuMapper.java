package com.demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.demo.model.Menu;

import java.util.List;

@Mapper
public interface MenuMapper {
    List<Menu> getAllMenus();
}
