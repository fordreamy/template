package com.demo;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Objects;
import java.util.Properties;

public class ReadProperties {
    public void read() {
        System.out.println("come in!");
        String rootPath = Objects.requireNonNull(this.getClass().getResource("/")).getPath();
        System.out.println("当前文件路径:" + rootPath);
        //读取配置文件
        Properties properties = new Properties();
        File file = new File(rootPath + "/application.properties");
        FileInputStream fis;
        try {
            fis = new FileInputStream(file);
            properties.load(fis);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //获取配置文件数据
        Enumeration<?> enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement().toString();
            System.out.println("key:" + key + ",值:" + properties.getProperty(key));
        }
    }
}
