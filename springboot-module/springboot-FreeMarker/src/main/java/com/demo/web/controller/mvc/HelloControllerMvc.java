package com.demo.web.controller.mvc;

import com.demo.web.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HelloControllerMvc {

    @GetMapping("/hi")
    @ResponseBody
    public String say() {
        return "Hello world!";
    }

    @RequestMapping("/admin")
    public @ResponseBody
    User u(User u) {
        return new User(u.getName(), 20);
    }

    @RequestMapping("/m1")
    public String model1(@RequestParam(name = "name", required = false, defaultValue = "jim") String name, Model model) {
        model.addAttribute("u", new User(name, 21));
        return "index";
    }


    @RequestMapping("/m2")
    public ModelAndView model2(ModelAndView modelAndView) {
        //ModelAndView也可在方法中构造,ModelAndView m = new ModelAndView()
        modelAndView.addObject("u", new User("tom", 21));
        modelAndView.setViewName("index");
        return modelAndView;
    }
}
