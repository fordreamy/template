package com.demo.web.controller.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloControllerRest {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @RequestMapping(value = "/r/{id}", method = RequestMethod.GET)
    public int index(@PathVariable int id) {
        return id;
    }
}
