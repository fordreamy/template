package com.demo.entity;

import lombok.Data;

/**
 * @author tiger
 */
@Data
public class Book {
    private Integer id;
    private String name;
    private String author;
}
