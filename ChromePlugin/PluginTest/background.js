/*
  Displays a notification with the current time. Requires "notifications"
  permission in the manifest file (or calling
  "Notification.requestPermission" beforehand).
*/
function show() {
    console.log('执行定时:' + new Date());
    
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://127.0.0.1:10086/show/sendMsg", true);
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
        var resp = JSON.parse(xhr.responseText);
        
       if(resp.length>0){
           for (let key in resp.data) {
               console.log(key, resp.data[key])
               	   
               var time = /(..)(:..)/.exec(new Date());     // The prettyprinted time.
               var hour = time[1] % 12 || 12;               // The prettyprinted hour.
               var period = time[1] < 12 ? 'a.m.' : 'p.m.'; // The period of the day.
               // 发送通知
               new Notification(hour + time[2] + ' ' + period, {
                   icon: 'img/logo.png',
                   body: resp.data[key]
               });
           }
       }
        
      }
    }
    xhr.send();

}

// Conditionally initialize the options.
if (!localStorage.isInitialized) {
  localStorage.isActivated = true;   // The display activation.
  localStorage.frequency = 1;        // The display frequency, in minutes.
  localStorage.isInitialized = true; // The option initialization.
}

// Test for notification support.
if (window.Notification) {
  // While activated, show notifications at the display frequency.
  if (JSON.parse(localStorage.isActivated)) { show(); }

  var interval = 0; // The display interval, in minutes.

  setInterval(function() {
    interval++;

    if (
      JSON.parse(localStorage.isActivated) &&
        localStorage.frequency <= interval
    ) {
      show();
      interval = 0;
    }
  }, 5000);
}