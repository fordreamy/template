package com.demo.mapper;

import com.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import com.demo.model.Role;

import java.util.List;

@Mapper
public interface UserMapper {
    User loadUserByUsername(String username);

    List<Role> getUserRolesByUid(Integer id);
}
