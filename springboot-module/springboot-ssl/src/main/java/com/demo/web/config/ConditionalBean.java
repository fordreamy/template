package com.demo.web.config;

import com.demo.web.service.MyService;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
//@ConditionalOnBean(name = "dataSource")
@ConditionalOnBean(MyService.class)
public class ConditionalBean {

    @Bean
    public void cb() {
        System.out.println("初始化DataSource后才初始化此Bean");
    }

}
