package com.demo.web.service.impl;

import java.util.Map;

import com.demo.web.dao.UserDao;
import com.demo.web.entity.User;
import com.demo.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    public User geUserById(Long id) {
        User user = userDao.findUserById(id);
        return user;
    }
}
