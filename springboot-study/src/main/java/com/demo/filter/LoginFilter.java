package com.demo.filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author tiger
 */
@Component
public class LoginFilter implements Filter {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public void init(FilterConfig filterConfig) {
        logger.info("过滤器初始化");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        logger.info("过滤器Filter拦截:" + req.getRequestURI());
        filterChain.doFilter(req, resp);
    }

    @Override
    public void destroy() {
        logger.info("该过滤器destroy了");
    }
}
