package initDB.initMysql;

import java.sql.Connection;

import initDB.DBConnection;
import initDB.DBScripRunner;
import initDB.initOracle.InitOracle;

/**
 * 初始化数据库
 * 
 * @author
 *
 */
public class InitMysql {

	public static void main(String[] args) {
		System.out.println("类根目录:" + ClassLoader.getSystemResource(""));
		System.out.println("当前类路径:" + InitOracle.class.getResource("").getPath()); // Class文件所在路径

		String filePath = InitMysql.class.getResource("").getPath() + "initMysql.sql";

		Connection conn = null;
		try {
			//不加?useUnicode=true&characterEncoding=utf8中文会有乱码
			conn = DBConnection.getConnection("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/test_01?useUnicode=true&characterEncoding=utf8","root", "123456");
		} catch (Exception e) {
			System.out.println("mysql链接失败!");
			e.printStackTrace();
		}
		
		DBScripRunner.sqlBatch(filePath, conn);
	}

}
