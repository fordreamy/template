// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
'use strict';


chrome.notifications.onButtonClicked.addListener(async () => {
	const delay = await chrome.storage.sync.get(['delay']);
	console.log('点击通知' + delay)
});

chrome.alarms.onAlarm.addListener(() => {
	var t =   ''
		chrome.storage.sync.get(['onOff']).then((r)=>t = r)
	
		console.log(t)
	chrome.action.setBadgeText({ text: t });
	console.log('start exec:' + new Date());
	fetch('http://127.0.0.1:10086/show/sendMsg', {
			method: 'GET',
			headers: {
				'content-type': 'application/json'
			},
			cache: 'no-cache',
			credentials: 'same-origin',
			mode: 'cors',
			redirect: 'follow',
			referrer: 'no-referrer'
		})
		.then(function(response) {
			console.log(response)
			return response.json();
		})
		.then(function(resp) {
			console.log(resp);
			if (resp.length > 0) {
				for (let key in resp.data) {
					console.log(key, resp.data[key])
					var time = /(..)(:..)/.exec(new Date()); // The prettyprinted time.
					var hour = time[1] % 12 || 12; // The prettyprinted hour.
					var period = time[1] < 12 ? 'a.m.' : 'p.m.'; // The period of the day.
					// 发送通知
					chrome.notifications.create({
						type: 'basic',
						iconUrl: 'stay_hydrated.png',
						title: hour + time[2] + ' ' + period,
						message: resp.data[key],
						buttons: [{
							title: 'Keep it Flowing.'
						}],
						priority: 0
					});
				}
			}
		});
});