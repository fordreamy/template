package com.demo.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author tiger
 */
@Data
@Entity(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String author;
}
