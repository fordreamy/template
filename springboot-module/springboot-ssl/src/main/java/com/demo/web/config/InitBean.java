package com.demo.web.config;

import com.demo.web.service.MyService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitBean {

    @Bean
    public MyService getMyServiceYa() {
        System.out.println("getMyService---------------------");
        return new MyService();
    }

}
