package com.demo.service.impl;

import com.demo.service.UserService;
import com.demo.dao.UserDao;
import com.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    public User geUserById(Long id) {
        User user = userDao.findUserById(id);
        return user;
    }
}
