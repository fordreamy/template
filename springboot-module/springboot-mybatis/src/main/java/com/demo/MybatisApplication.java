package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tiger
 * ＠SpringBootApplication主要包含三个注解。
 * ①＠SpringBootConfiguration表明这是个配置类，开发者可以在这个类中配置 Bean。
 * ②＠EnableAutoConfiguration表示开启自动化配置。
 * ③＠ComponentScan 完成包扫描，Spring中的功能 由于＠ComponentScan默认扫描的类都位于当前类所在包下面，建议在实际项目开发中项目启动类放在根包中
 */
@SpringBootApplication
public class MybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisApplication.class, args);
    }

   /* @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

        };
    }*/

}
