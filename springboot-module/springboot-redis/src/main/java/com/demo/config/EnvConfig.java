package com.demo.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author tiger
 */
@Configuration
public class EnvConfig {

    private final Environment env;

    public EnvConfig(Environment env) {
        Log logger = LogFactory.getLog(this.getClass());
        logger.info("获得环境Environment ^_^");
        this.env = env;
    }

    @Bean
    public void getServerPort() {
        System.out.println(env.getProperty("user.dir"));
        System.out.println(env.getProperty("JAVA_HOME"));
        System.out.println(env.getProperty("server.port"));
        InetAddress address;
        try {
            address = InetAddress.getLocalHost();
            String ip = address.getHostAddress();
            String port = env.getProperty("server.port");
            String contextPath = env.getProperty("server.servlet.context-path");
            System.out.println("IP:" + ip);
            System.out.println("主机名:" + address.getHostName());
            System.out.println("端口:" + port);
            System.out.println();
            System.out.println("访问URL: http://" + ip + ":" + port + contextPath);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

}
