package com.demo.web.controller.mvc;

import com.demo.web.entity.User;
import com.demo.web.service.MyService;
import com.demo.web.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {
    private Log log = LogFactory.getLog(UserController.class);
    @Autowired
    UserService userService;

    @Autowired
    MyService myService;

    @RequestMapping("/user/{id}")
    @ResponseBody
    public User getUserById(@PathVariable Long id) {
        log.info("执行getUserById");
        System.out.println(myService.getName());
        User user = userService.geUserById(id);
        return user;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/post/user/{id}")
    @ResponseBody
    public User getUserByIdPost(@PathVariable Long id) {
        User user = userService.geUserById(id);
        return user;
    }
}
