package com.demo.service;

import com.demo.dao.BookDao;
import com.demo.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tiger
 */
@Service
public class BookService {
    @Resource
    BookDao bookDao;

    public Page<Book> getBookByPage(Pageable pageable) {
        return bookDao.findAll(pageable);
    }

    public void addBook(Book book) {
        bookDao.save(book);
    }

    public void updateBook(Book book) {
        bookDao.save(book);
    }

    public void deleteBookById(Integer id) {
        bookDao.deleteById(id);
    }

    public List<Book> getBookByName(String name) {
        return bookDao.getBookByName(name);
    }

    public List<Book> getAllBooks() {
        return bookDao.findAll();
    }

}
