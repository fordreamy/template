package com.demo.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class EnvConfig {

    private final Log logger = LogFactory.getLog(this.getClass());

    private Environment env;

    public EnvConfig(Environment env) {
        logger.info("获得环境Environment ^_^");
        this.env = env;
    }

    @Bean
    public void getServerPort() {
        System.out.println(env.getProperty("user.dir"));
        System.out.println(env.getProperty("JAVA_HOME"));
        System.out.println(env.getProperty("server.port"));
    }

}
