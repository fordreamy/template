package com.demo.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloControllerRest {

    private final Log logger = LogFactory.getLog(this.getClass());

    @RequestMapping(value = "/r/{id}", method = RequestMethod.GET)
    public int index(@PathVariable int id) {
        logger.info("进入rest");
        return id;
    }
}
