drop table user if exists;
CREATE TABLE user
(
    id       int(11),
    username varchar(45),
    password varchar(255),
    enabled  tinyint(1),
    locked   tinyint(11),
    PRIMARY KEY (id)
);

drop table role if exists;
CREATE TABLE role
(
    id     int(11),
    name   varchar(45),
    nameZh varchar(45),
    PRIMARY KEY (id)
);

drop table menu if exists;
CREATE TABLE menu
(
    id      int(11),
    pattern varchar(45)
);

drop table menu_role if exists;
CREATE TABLE menu_role
(
    id  int(11),
    mid int(11),
    rid int(11)
);

drop table user_role if exists;
CREATE TABLE user_role
(
    id  int(11),
    uid int(11),
    rid int(11)
);

drop table book if exists;
CREATE TABLE book
(
    id     int(11),
    name   varchar(100),
    author varchar(100),
    PRIMARY KEY (id)
);
