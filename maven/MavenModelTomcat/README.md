## 这是一个使用maven的web项目模板，在创建项目时直接拷贝即可 

### 本项目环境(其它亦可): 
	Project Facet      Version
	Dynamic Web Module 3.0
	Maven              apache-maven-3.3.9
	Java               1.7
	Tomcat             7
	MySQL              5.1.21
	Oracle             10.2.0.5.0

### 编译运行项目

`1.使用插件jetty-maven-plugin(版本9.2.20.v20161216)`  
　　mvn jetty:run  
　　配置Eclipse  
　　　　打开Run Configurations  
　　　　新建Maven Build  
　　　　Base directory：${workspace_loc:/${project_name}}  
　　　　Goals:jetty:run  
　　　　User settings: maven\...\setting.xml  
　　　　Maven Runtime: maven安装路径  
`2.使用插件tomcat7-maven-plugin(版本2.2)`  
　　Goals:tomcat7:run  
　　当不是标准maven目录(src/main/webapp等)时且用<Context>标签配置WebRoot时,classes目录应为WEB-INF下  
`3.执行URL：http://localhost:8080/MavenModel2`
    
### 数据库初始化

	通过hibernate.cfg.xml自动创建表,共3张表,user用户表,role角色表,userRole用户角色表
	数据库初始化
		H2 database(支持嵌入式、内存数据库，执行InitH2.java)
		MySql database(执行InitMysql.java利用ibatis执行sql脚本.sql文件)
		



 
	

