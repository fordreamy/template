package com.demo.service;

import com.demo.dao.UserDao;
import com.demo.entity.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author tiger
 */
@Service
public class UserService {
    @Resource
    UserDao userDao;

    /**
     * 获取用户信息
     *
     * @param id 　用户ID
     * @return 用户信息
     */
    public User geUserById(Long id) {
        return userDao.findUserById(id);
    }


    /**
     * 获取用户列表
     *
     * @return 用户列表
     */
    public List<Map<String, Object>> geUserList() {
        return userDao.geUserList();
    }

}
