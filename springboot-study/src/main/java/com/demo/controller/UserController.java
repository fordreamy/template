package com.demo.controller;

import com.demo.service.UserService;
import com.demo.entity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author tiger
 */
@Controller
public class UserController {
    private final Log log = LogFactory.getLog(UserController.class);
    @Autowired
    UserService userService;

    @GetMapping("/")
    public String home() {
        log.info("进入首页");
        return "index.html";
    }

    @GetMapping("/user")
    @ResponseBody
    public  List<Map<String, Object>> getUserList() {
        return userService.geUserList();
    }

    @GetMapping("/user/{id}")
    @ResponseBody
    public User getUserById(@PathVariable Long id) {
        return userService.geUserById(id);
    }
}
