package com.demo;

import com.demo.entity.Book;
import com.demo.service.BookService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
public class Test01ApplicationTests {

    @Autowired
    BookService bookService;

    @Autowired
    WebApplicationContext wac;
    MockMvc mockMvc;

    @Before
    public void before() {
        System.out.println("before");
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void serviceTest() {
        Book book = new Book(1, "三国演义", "罗贯中");
        bookService.addBook(book);
        System.out.println(bookService.getAllBooks());
    }

    @Test
    public void controllerTestGet() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/books")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("name", "Michael"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
    }

}
