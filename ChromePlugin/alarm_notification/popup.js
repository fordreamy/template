// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
'use strict';

const form = document.querySelector('.create-alarm');

function clearAlarm() {
  chrome.action.setBadgeText({ text: 'OFF' });
  chrome.storage.sync.set({ onOff: 'OFF' });
  chrome.alarms.clearAll();
  // window.close();
}

// New alarm form

form.addEventListener('submit', (event) => {
	
  console.log('set alarm')
  event.preventDefault();
  const formData = new FormData(form);
  const data = Object.fromEntries(formData);

  // Extract form values
  const delay = Number.parseFloat(data['delay']);
  const period = Number.parseFloat(data['period']);
  
  console.log('delay->'+delay)
  console.log('period->'+period)
	
  const minutes = parseFloat(event.target.value);
  chrome.action.setBadgeText({ text: 'ON' });
  chrome.storage.sync.set({ onOff: 'ON' });
  
  // 创建一个提醒，periodInMinutes间隔N分钟运行一次， delayInMinutes延时N分后执行
  chrome.alarms.create({ delayInMinutes: delay, periodInMinutes:period });
  
  chrome.storage.sync.set({ delay: delay });
  chrome.storage.sync.set({ period: period });
  // window.close();
  
});

// An Alarm delay of less than the minimum 1 minute will fire
// in approximately 1 minute increments if released
document.getElementById('cancelAlarm').addEventListener('click', clearAlarm);
