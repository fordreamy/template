package com.demo.web.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {

    @PostMapping("upload")
    @ResponseBody
    public String handFormUpload(String name, MultipartFile file) {
        if (!file.isEmpty()) {
            System.out.println("文件名:" + file.getOriginalFilename());
            System.out.println("文件大小:" + file.getSize());
        }
        return "success";
    }
}
