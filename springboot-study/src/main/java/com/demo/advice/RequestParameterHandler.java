package com.demo.advice;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletResponse;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

/**
 * @author tiger
 * ＠ControllerAdvice 就是＠Controller 增强版。＠ControllerAdvice 主要用来处理全
 * 局数据，一般搭配＠ExceptionHandler、＠ModelAttribute 及＠InitBinder 使用。
 */
@ControllerAdvice
public class RequestParameterHandler {

    /**
     * 注解@ControllerAdvice 结合＠InitBinder 还能实现请求参数预处理，即将表单中的数据绑定到实体类上时进行一些额外处理。
     * 将页面的日期参数转换成实体的日期对象
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        System.out.println("求参数类型转换");
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue("ss");
            }
        });
    }
}
