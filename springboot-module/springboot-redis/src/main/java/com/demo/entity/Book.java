package com.demo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tiger
 */
@Data
public class Book implements Serializable {
    private Integer id;
    private String name;
    private String author;
}
